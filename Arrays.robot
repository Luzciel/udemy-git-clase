*** Settings ***
Library    String
Library    SeleniumLibrary

*** Variables ***
${browser}    chrome
${homepage}   automationpractice.com/index.php
${scheme}     http
${TestUrl}    ${scheme}://${homepage}

*** Keywords ***
Open homepage
     Open Browser    ${TestUrl}    ${browser}

*** Test Cases ***
001 Hacer Clic en Contenedores
    Open homepage
    Set Global Variable    @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${NombreDeContenedor}    IN    @{nombresDeContenedores}
    \   Click Element    xpath=${NombreDeContenedor}
    \   Wait Until Element Is Visible    xpath=//*[@id="bigpic"]
    \   Click Element    xpath=//*[@id="header_logo"]/a/img
    Close Browser

C002 Caso de prueba nuevo Abrir Pag
    Open homepage
